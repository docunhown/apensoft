﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

namespace APENSoft
{
    public partial class SizeExplorer : Form
    {
        public SizeExplorer()
        {
            this.InitializeComponent();
        }

        private void SaveSize_Click(object sender, EventArgs e)
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\APEN\APENSoft");
            if (this.SizeEnabled.Checked == true)
            {
                key.SetValue("SizeActive", "1");
            }
            else
            {
                key.SetValue("SizeActive", "0");
            }
            key.Close();
        }
    }
}