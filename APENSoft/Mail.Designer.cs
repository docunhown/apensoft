﻿namespace APENSoft
{
    partial class Mail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mail));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txbSenderAdress = new System.Windows.Forms.TextBox();
            this.txbRecieverAdress = new System.Windows.Forms.TextBox();
            this.txbServer = new System.Windows.Forms.TextBox();
            this.txbPort = new System.Windows.Forms.TextBox();
            this.txbPassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txbMachine = new System.Windows.Forms.TextBox();
            this.ConnectionTest = new System.Windows.Forms.Button();
            this.SaveMail = new System.Windows.Forms.Button();
            this.MailEnable = new System.Windows.Forms.CheckBox();
            this.WarningFields = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(234, 168);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sender Login";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(234, 242);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(222, 32);
            this.label2.TabIndex = 1;
            this.label2.Text = "Reciever Adress";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(234, 313);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(197, 32);
            this.label3.TabIndex = 2;
            this.label3.Text = "Sender Server";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(234, 391);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(166, 32);
            this.label4.TabIndex = 3;
            this.label4.Text = "Sender Port";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(234, 462);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(238, 32);
            this.label5.TabIndex = 4;
            this.label5.Text = "Sender Password";
            // 
            // txbSenderAdress
            // 
            this.txbSenderAdress.Location = new System.Drawing.Point(478, 168);
            this.txbSenderAdress.Name = "txbSenderAdress";
            this.txbSenderAdress.Size = new System.Drawing.Size(384, 38);
            this.txbSenderAdress.TabIndex = 5;
            // 
            // txbRecieverAdress
            // 
            this.txbRecieverAdress.Location = new System.Drawing.Point(478, 239);
            this.txbRecieverAdress.Name = "txbRecieverAdress";
            this.txbRecieverAdress.Size = new System.Drawing.Size(384, 38);
            this.txbRecieverAdress.TabIndex = 6;
            // 
            // txbServer
            // 
            this.txbServer.Location = new System.Drawing.Point(478, 313);
            this.txbServer.Name = "txbServer";
            this.txbServer.Size = new System.Drawing.Size(384, 38);
            this.txbServer.TabIndex = 7;
            // 
            // txbPort
            // 
            this.txbPort.Location = new System.Drawing.Point(478, 388);
            this.txbPort.Name = "txbPort";
            this.txbPort.Size = new System.Drawing.Size(384, 38);
            this.txbPort.TabIndex = 8;
            // 
            // txbPassword
            // 
            this.txbPassword.Location = new System.Drawing.Point(478, 459);
            this.txbPassword.Name = "txbPassword";
            this.txbPassword.Size = new System.Drawing.Size(384, 38);
            this.txbPassword.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(234, 527);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(205, 32);
            this.label6.TabIndex = 10;
            this.label6.Text = "Machine Name";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // txbMachine
            // 
            this.txbMachine.Location = new System.Drawing.Point(478, 521);
            this.txbMachine.Name = "txbMachine";
            this.txbMachine.Size = new System.Drawing.Size(384, 38);
            this.txbMachine.TabIndex = 11;
            // 
            // ConnectionTest
            // 
            this.ConnectionTest.Location = new System.Drawing.Point(349, 647);
            this.ConnectionTest.Name = "ConnectionTest";
            this.ConnectionTest.Size = new System.Drawing.Size(197, 85);
            this.ConnectionTest.TabIndex = 12;
            this.ConnectionTest.Text = "Test Connection";
            this.ConnectionTest.UseVisualStyleBackColor = true;
            this.ConnectionTest.Click += new System.EventHandler(this.ConnectionTest_Click);
            // 
            // SaveMail
            // 
            this.SaveMail.Location = new System.Drawing.Point(625, 647);
            this.SaveMail.Name = "SaveMail";
            this.SaveMail.Size = new System.Drawing.Size(196, 85);
            this.SaveMail.TabIndex = 13;
            this.SaveMail.Text = "Save Settings";
            this.SaveMail.UseVisualStyleBackColor = true;
            this.SaveMail.Click += new System.EventHandler(this.SaveMail_Click);
            // 
            // MailEnable
            // 
            this.MailEnable.AutoSize = true;
            this.MailEnable.Location = new System.Drawing.Point(349, 73);
            this.MailEnable.Name = "MailEnable";
            this.MailEnable.Size = new System.Drawing.Size(228, 36);
            this.MailEnable.TabIndex = 14;
            this.MailEnable.Text = "Enable Mailer";
            this.MailEnable.UseVisualStyleBackColor = true;
            // 
            // WarningFields
            // 
            this.WarningFields.AutoSize = true;
            this.WarningFields.ForeColor = System.Drawing.Color.Red;
            this.WarningFields.Location = new System.Drawing.Point(472, 612);
            this.WarningFields.Name = "WarningFields";
            this.WarningFields.Size = new System.Drawing.Size(289, 32);
            this.WarningFields.TabIndex = 15;
            this.WarningFields.Text = "Please fill in all fields!";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(137, 580);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(873, 32);
            this.label7.TabIndex = 16;
            this.label7.Text = "To add multiple Recievers use \"user1@apen.es;user2@apen.es[...]\"";
            // 
            // Mail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 758);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.WarningFields);
            this.Controls.Add(this.MailEnable);
            this.Controls.Add(this.SaveMail);
            this.Controls.Add(this.ConnectionTest);
            this.Controls.Add(this.txbMachine);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txbPassword);
            this.Controls.Add(this.txbPort);
            this.Controls.Add(this.txbServer);
            this.Controls.Add(this.txbRecieverAdress);
            this.Controls.Add(this.txbSenderAdress);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Mail";
            this.Text = "Mail Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txbSenderAdress;
        private System.Windows.Forms.TextBox txbRecieverAdress;
        private System.Windows.Forms.TextBox txbServer;
        private System.Windows.Forms.TextBox txbPort;
        private System.Windows.Forms.TextBox txbPassword;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txbMachine;
        private System.Windows.Forms.Button ConnectionTest;
        private System.Windows.Forms.Button SaveMail;
        private System.Windows.Forms.CheckBox MailEnable;
        private System.Windows.Forms.Label WarningFields;
        private System.Windows.Forms.Label label7;
    }
}