﻿namespace APENSoft
{
    partial class SizeExplorer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SizeExplorer));
            this.SizeEnabled = new System.Windows.Forms.CheckBox();
            this.SaveSize = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SizeEnabled
            // 
            this.SizeEnabled.AutoSize = true;
            this.SizeEnabled.Location = new System.Drawing.Point(174, 152);
            this.SizeEnabled.Name = "SizeEnabled";
            this.SizeEnabled.Size = new System.Drawing.Size(319, 36);
            this.SizeEnabled.TabIndex = 0;
            this.SizeEnabled.Text = "Enable Size Explorer";
            this.SizeEnabled.UseVisualStyleBackColor = true;
            // 
            // SaveSize
            // 
            this.SaveSize.Location = new System.Drawing.Point(174, 244);
            this.SaveSize.Name = "SaveSize";
            this.SaveSize.Size = new System.Drawing.Size(232, 66);
            this.SaveSize.TabIndex = 1;
            this.SaveSize.Text = "Save Settings";
            this.SaveSize.UseVisualStyleBackColor = true;
            this.SaveSize.Click += new System.EventHandler(this.SaveSize_Click);
            // 
            // SizeExplorer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 517);
            this.Controls.Add(this.SaveSize);
            this.Controls.Add(this.SizeEnabled);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SizeExplorer";
            this.Text = "SizeExplorer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox SizeEnabled;
        private System.Windows.Forms.Button SaveSize;
    }
}