﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Win32;

namespace APENSoft
{
    public partial class Form1 : Form
    {
        private int _mailactive = 0;
        private int _logactive = 0;
        private int _counteractive = 0;
        private int _sizeactive = 0;
        private string _sendermail;
        private string _senderpass;
        private string _senderserver;
        private string _recievermail;
        private string _logdir;
        private string _counterdir;

        //TODO Auto Updater
        //TODO General Settings -> Reg in Local User oder System
        public Form1(string args)
        {
            this.InitializeComponent();

            Timer clock = new Timer { Interval = 1 };
            clock.Tick += new EventHandler(this.timer_tick);
            clock.Start();
            ProcessRunner AutoRunner = new ProcessRunner();
            AutoRunner = null;
            if (args != String.Empty)
            {
                if (args == "/c")
                {
                    Environment.Exit(1);
                }
            }
        }

        private void timer_tick(object sender, EventArgs e)
        {
            RegistryKey reg = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\APEN\APENSoft");
            bool mailFeatureActive = false;
            bool logFeatureActive = false;
            bool sizeFeatureActive = false;
            bool counterFeatureActive = false;

            if (Convert.ToString(reg?.GetValue("MaiFeature")) == "1")
            {
                mailFeatureActive = true;
            }
            if (Convert.ToString(reg?.GetValue("LogFeature")) == "1")
            {
                logFeatureActive = true;
            }
            if (Convert.ToString(reg?.GetValue("SizeActive")) == "1")
            {
                sizeFeatureActive = true;
            }
            if (Convert.ToString(reg?.GetValue("CounterActive")) == "1")
            {
                counterFeatureActive = true;
            }

            if (Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\APEN\APENSoft", "CounterActive", null) == null)
            {
                if (reg != null)
                {
                    reg.SetValue("LogFeature", 0);
                    reg.SetValue("SizeActive", 0);
                    reg.SetValue("CounterActive", 0);
                    reg.SetValue("CounterType", 0);
                    reg.SetValue("SenderMail", "null");
                    reg.SetValue("SenderServer", "null");
                    reg.SetValue("SenderPassword", "null");
                    reg.SetValue("RecieverMail", "null");
                    reg.SetValue("LogDirectory", "null");
                    reg.SetValue("CounterDirectory", "null");
                    reg.SetValue("ServerName", "null");
                    reg.SetValue("ServerPort", "null");
                }
            }
            else
            {
            }

            switch (mailFeatureActive)
            {
                case true:
                    string[] mailData = this.MailInfo();
                    this.MailActive.ForeColor = Color.Lime;
                    this.MailActive.Text = "Activated";
                    this.labelSender.Show();
                    this.labelReciever.Show();
                    this.Sender.Show();
                    this.Sender.Text = mailData[0];
                    this.Reciever.Show();
                    this.Reciever.Text = mailData[3];
                    break;

                case false:
                    this.MailActive.ForeColor = Color.Red;
                    this.MailActive.Text = "Disabled";
                    this.Sender.Hide();
                    this.Reciever.Hide();
                    this.labelSender.Hide();
                    this.labelReciever.Hide();
                    break;
            }
            switch (logFeatureActive)
            {
                case true:
                    this.logActive.ForeColor = Color.Lime;
                    this.logActive.Text = "Activated";
                    this.labelLogDir.Show();
                    this.LogDir.Show();
                    this.LogDir.Text = (string)reg.GetValue("LogDirectory");
                    break;

                case false:
                    this.logActive.ForeColor = Color.Red;
                    this.logActive.Text = "Disabled";
                    this.labelLogDir.Hide();
                    this.LogDir.Hide();
                    break;
            }
            switch (sizeFeatureActive)
            {
                case true:
                    this.ExplorerActive.ForeColor = Color.Lime;
                    this.ExplorerActive.Text = "Activated";
                    break;

                case false:
                    this.ExplorerActive.ForeColor = Color.Red;
                    this.ExplorerActive.Text = "Disabled";
                    break;
            }
            switch (counterFeatureActive)
            {
                case true:
                    this.CounterActive.ForeColor = Color.Lime;
                    this.CounterActive.Text = "Activated";
                    this.labelCounterDir.Show();
                    this.labelCounterType.Show();
                    this.CounterDir.Show();
                    string toll = Convert.ToString((reg.GetValue("CounterDirectory")));
                    CounterDir.Text = toll;
                    string converted = Convert.ToString(reg.GetValue("CounterType"));
                    bool type = false;
                    switch (converted)
                    {
                        case "1":
                            type = true;
                            break;

                        case "0":
                            type = false;

                            break;
                    }
                    this.CounterType.Show();
                    switch (type)
                    {
                        case true:
                            this.CounterType.Text = "Top Folder only";
                            break;

                        case false:
                            this.CounterType.Text = "Top- and Childfolders";
                            break;
                    }
                    break;

                case false:
                    this.CounterActive.ForeColor = Color.Red;
                    this.CounterActive.Text = "Disabled";
                    this.CounterDir.Hide();
                    this.CounterType.Hide();
                    this.labelCounterDir.Hide();
                    this.labelCounterType.Hide();
                    break;
            }
            reg.Close();
        }

        public string[] MailInfo()
        {
            RegistryKey reg = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\APEN\APENSoft");
            string[] info = new string[4];
            info[0] = (string)reg.GetValue("SenderMail");
            info[1] = (string)reg.GetValue("SenderServer");
            info[2] = (string)reg.GetValue("SenderPassword");
            info[3] = (string)reg.GetValue("RecieverMail");
            reg.Close();
            return info;
        }

        private void Easteregg()
        {
            MessageBox.Show("");
        }

        private void fileCounterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Counter frmCounter = new Counter();
            frmCounter.Show();
        }

        private void sizeExplorerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SizeExplorer frmSizeExplorer = new SizeExplorer();
            frmSizeExplorer.Show();
        }

        private void easterEggToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Easteregg();
        }

        private void mailSetingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mail frmMail = new Mail();
            frmMail.Show();
        }

        private void logSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Log frmLog = new Log();
            frmLog.Show();
        }

        private void LaunchButton_Click(object sender, EventArgs e)
        {
            ProcessRunner buttonRunner = new ProcessRunner();
            buttonRunner = null;
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void liceseInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You can find the License Information here:\nhttp://bit.ly/APENLic");
        }
    }
}