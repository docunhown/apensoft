﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Windows.Forms;
using Microsoft.Win32;

namespace APENSoft
{
    internal class ProcessRunner
    {
        private string[] _textFile = new string[99];
        private List<string> File = new List<string>();
        private List<string> Size = new List<string>();
        private string[] _textSize = new string[99];
        private RegistryKey _reg = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\APEN\APENSoft", true);

        public ProcessRunner()
        {
            this._textFile[0] = "1";
            string sizeActive = Convert.ToString(this._reg.GetValue("SizeActive"));
            string counterActive = Convert.ToString(this._reg.GetValue("CounterActive"));
            string mailActive = Convert.ToString(this._reg.GetValue("MailFeature"));
            string logActive = Convert.ToString(this._reg.GetValue("LogFeature"));
            if (sizeActive == "1")
            {
                try
                {
                    this.SizeRunner();
                }
                catch (Exception e)
                {
                    MessageBox.Show("Size-Explorer Error!\n " + e);
                    throw;
                }
            }
            if (counterActive == "1")
            {
                string path = Convert.ToString(this._reg.GetValue("CounterDirectory"));
                bool type = Convert.ToString(this._reg.GetValue("CounterType")) == "1";
                try
                {
                    this.FileRunner(path, type);
                }
                catch (Exception e)
                {
                    MessageBox.Show("File-Counter Error!\n" + e);
                    throw;
                }
            }
            if (mailActive == "1")
            {
                string sender = Convert.ToString(this._reg.GetValue("SenderMail"));
                string reciever = Convert.ToString(this._reg.GetValue("RecieverMail"));
                string server = Convert.ToString(this._reg.GetValue("SenderServer"));
                string password = Convert.ToString(this._reg.GetValue("SenderPassword"));
                int port = Convert.ToInt32(this._reg.GetValue("ServerPort"));
                try
                {
                    this.MailRunner(sender, reciever, server, password, port);
                }
                catch (Exception e)
                {
                    MessageBox.Show("Mail-Error!\n" + e);
                    throw;
                }
            }
            if (logActive == "1")
            {
                try
                {
                    this.LogRunner(Convert.ToString(this._reg.GetValue("LogDirectory")));
                }
                catch (Exception e)
                {
                    MessageBox.Show("Log-Error!\n" + e);
                    throw;
                }
            }
        }

        public void SizeRunner()
        {
            DateTime now = DateTime.Now;
            this._textSize[0] = ("Disk snapshot at " + now);
            int i = 1;
            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                //Write The Drive Letter
                if (drive.DriveType != DriveType.Fixed) continue;
                this.Size.Add(drive.Name + "\t");

                //Get the Data in Bytes
                long all = drive.TotalSize;
                long free = drive.AvailableFreeSpace;
                //Convert to Gigabýtes
                double gbFree = free / 1024 / 1024 / 1024;
                double gbTotal = all / 1024 / 1024 / 1024;
                //Write to File
                this.Size.Add(gbFree + " GB free at a total of " + gbTotal + " GB of Space");
                this.Size.Add("");
            }
            this.Size.Add("========================================================");
        }

        public void FileRunner(string path, bool art)
        {
            //TODO File Access Check

            string dirPoiter = path;
            switch (art)
            {
                case true:
                    {
                        this.File.Add("File snapshot at " + DateTime.Now);
                        this.File.Add(dirPoiter + "     :     " + Directory.GetFiles(dirPoiter, "*", SearchOption.TopDirectoryOnly).Length);
                        this.File.Add("--------------------------------------------------------");
                        this.File.Add("========================================================");
                    }
                    break;

                case false:
                    {
                        this.File.Add("File snapshot at" + DateTime.Now);
                        this.File.Add(dirPoiter + "     :     " + Directory.GetFiles(dirPoiter, "*", SearchOption.AllDirectories).Length);
                        this.File.Add("--------------------------------------------------------");
                        this.File.Add("Counting Method:    Top- & Subdirectorys");
                        this.File.Add("========================================================");
                    }
                    break;
            }
        }

        public void MailRunner(string sender, string reciever, string senderServer, string senderPass, int senderPort)
        {
            List<string> reciepentsList = new List<string>();
            if (reciever.Contains(";"))
            {
                reciepentsList = reciever.Split(';').ToList();
            }
            else
            {
                reciepentsList.Add(reciever);
            }
            int ende = 0;
            string mailText = null;
            foreach (string i in this.File)
            {
                mailText = mailText + i + "\n";
            }
            ende = 0;
            mailText = mailText + "\n\n\n";
            foreach (string n in this.Size)
            {
                mailText = mailText + n + "\n";
            }
            string subject = "Status Report from " + Convert.ToString(this._reg.GetValue("ServerName"));
            try
            {
                SmtpClient mySmtpClient = new SmtpClient(senderServer);

                // Start SMTP Client
                mySmtpClient.UseDefaultCredentials = false;
                System.Net.NetworkCredential basicAuthenticationInfo = new
                    System.Net.NetworkCredential(sender, senderPass);
                mySmtpClient.Credentials = basicAuthenticationInfo;

                // Add From & To Adresses
                MailAddress from = new MailAddress("info@apen.de", "APEN SYS Information");
                MailAddress to = new MailAddress(reciepentsList[0]);
                MailMessage myMail = new System.Net.Mail.MailMessage(from, to);
                MailAddress cc = null;
                //Add CC Adresses when necessary
                if (reciepentsList.Count > 1)
                {
                    foreach (string strCc in reciepentsList)
                    {
                        cc = new MailAddress(strCc);
                        if (Equals(cc, to))
                        {
                            continue;
                        }
                        myMail.CC.Add(cc);
                    }
                }

                // Set Subject and Encoding of the Subject
                myMail.Subject = subject;
                myMail.SubjectEncoding = System.Text.Encoding.UTF8;

                // Set Body and Encoding of the Body
                myMail.Body = mailText;
                myMail.BodyEncoding = System.Text.Encoding.UTF8;
                // Set HTML Mode to OFF
                myMail.IsBodyHtml = false;

                mySmtpClient.Send(myMail);
            }
            catch (SmtpException ex)
            {
                throw new ApplicationException
                    ("SmtpException has occured: " + ex.Message);
            }
        }

        public void LogRunner(string dir)
        {
            string sizeActive = Convert.ToString(this._reg.GetValue("SizeActive"));
            string counterActive = Convert.ToString(this._reg.GetValue("CounterActive"));
            if (sizeActive == "1")
            {
                DateTime now = DateTime.Now;
                StreamWriter sizeInfo2 = new StreamWriter(dir + @"\size_info.txt", true);
                sizeInfo2.WriteLine("Disk snapshot at " + now);
                foreach (DriveInfo drive in DriveInfo.GetDrives())
                {
                    //Write The Drive Letter
                    Console.Write(drive.Name + $@"	");
                    if (drive.DriveType == DriveType.Fixed)
                    {
                        sizeInfo2.Write(drive.Name + "\t", true);

                        //Get the Data in Bytes
                        long all = drive.TotalSize;
                        long free = drive.AvailableFreeSpace;
                        //Convert to Gigabýtes
                        double gbFree = free / 1024 / 1024 / 1024;
                        double gbTotal = all / 1024 / 1024 / 1024;
                        //Write to File
                        sizeInfo2.Write(gbFree + " GB free at a total of " + gbTotal + " GB of Space");
                        sizeInfo2.WriteLine();
                    }
                }
                sizeInfo2.WriteLine("========================================================");

                sizeInfo2.Close();
            }
            if (counterActive == "1")
            {
                string path = Convert.ToString(this._reg.GetValue("CounterDirectory"));
                bool art = Convert.ToString(this._reg.GetValue("CounterType")) == "1";
                StreamWriter fileinfoWriter = new StreamWriter(dir + "\\" + "file_info.txt", true);
                DateTime now = DateTime.Now;
                string dirPoiter = path;
                switch (art)
                {
                    case true:
                        {
                            fileinfoWriter.WriteLine("File snapshot at" + now);
                            fileinfoWriter.Write(dirPoiter + "     :     ");
                            fileinfoWriter.Write(Directory.GetFiles(dirPoiter, "*", SearchOption.TopDirectoryOnly).Length);
                            fileinfoWriter.WriteLine();
                            fileinfoWriter.WriteLine("Counting Method:    Topdirecotry Only");
                            fileinfoWriter.WriteLine("========================================================");
                        }
                        break;

                    case false:
                        {
                            fileinfoWriter.WriteLine("File snapshot at" + now);
                            fileinfoWriter.Write(dirPoiter + "     :     ");
                            fileinfoWriter.Write(Directory.GetFiles(dirPoiter, "*", SearchOption.AllDirectories).Length);
                            fileinfoWriter.WriteLine();
                            fileinfoWriter.WriteLine("Counting Method:    Top- & Subdirectorys");
                            fileinfoWriter.WriteLine("========================================================");
                        }
                        break;
                }
                fileinfoWriter.Close();
            }
        }
    }
}