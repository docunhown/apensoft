﻿using System;
using System.Diagnostics.PerformanceData;
using System.Windows.Forms;
using Microsoft.Win32;

namespace APENSoft
{
    public partial class Counter : Form
    {
        private RegistryKey _reg = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\APEN\APENSoft", true);
        public string DirectoryString = "aaa";
        private bool _active = false;
        private int _type;

        public Counter()
        {
            this.DirectoryString = (string)this._reg.GetValue("CounterDirectory");
            InitializeComponent();
            Timer clock = new Timer { Interval = 1 };
            clock.Tick += this.timer_tick;
            clock.Start();
            ToolTip ttt = new ToolTip();
        }

        private void timer_tick(object sender, EventArgs e)
        {
            if (FolderBrowser.SelectedPath == null)
            {
            }
            else
            {
                this.DirectoryString = this.FolderBrowser.SelectedPath;
            }
            this.Directory.Text = this.DirectoryString;
            this.ButtonDisabler();
        }

        private void ChangeFolderButton_Click(object sender, EventArgs e)
        {
            this.FolderBrowser.ShowDialog();
        }

        private void ButtonDisabler()
        {
            if (this.DirectoryString == "null" || this.DirectoryString == "" && this.EnableCounter.CheckState == CheckState.Checked)
            {
                this.SaveButton.Enabled = false;
                this.labelWarning.Show();
            }
            else
            {
                this.SaveButton.Enabled = true;
                this.labelWarning.Hide();
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\APEN\APENSoft");

            switch (this.EnableCounter.Checked)
            {
                case true:
                    key.SetValue("CounterActive", "1");
                    break;

                default:
                    key.SetValue("CounterActive", "0");
                    break;
            }
            key.SetValue("CounterDirectory", this.DirectoryString);

            switch (this.TopFolder.Checked)
            {
                case true:
                    key.SetValue("CounterType", "1");
                    break;

                case false:
                    key.SetValue("CounterType", "0");
                    break;
            }
            key.Close();
        }

        private void FolderBrowser_HelpRequest(object sender, EventArgs e)
        {
        }
    }
}