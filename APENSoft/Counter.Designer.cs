﻿namespace APENSoft
{
    partial class Counter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Counter));
            this.EnableCounter = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AllFolders = new System.Windows.Forms.RadioButton();
            this.TopFolder = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.Directory = new System.Windows.Forms.Label();
            this.ChangeFolderButton = new System.Windows.Forms.Button();
            this.FolderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.SaveButton = new System.Windows.Forms.Button();
            this.labelWarning = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // EnableCounter
            // 
            this.EnableCounter.AutoSize = true;
            this.EnableCounter.Location = new System.Drawing.Point(352, 85);
            this.EnableCounter.Name = "EnableCounter";
            this.EnableCounter.Size = new System.Drawing.Size(305, 36);
            this.EnableCounter.TabIndex = 0;
            this.EnableCounter.Text = "Enable File Counter";
            this.EnableCounter.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.AllFolders);
            this.groupBox1.Controls.Add(this.TopFolder);
            this.groupBox1.Location = new System.Drawing.Point(194, 147);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(663, 177);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // AllFolders
            // 
            this.AllFolders.AutoSize = true;
            this.AllFolders.Location = new System.Drawing.Point(158, 105);
            this.AllFolders.Name = "AllFolders";
            this.AllFolders.Size = new System.Drawing.Size(310, 36);
            this.AllFolders.TabIndex = 1;
            this.AllFolders.Text = "Top- and Childfolder";
            this.AllFolders.UseVisualStyleBackColor = true;
            // 
            // TopFolder
            // 
            this.TopFolder.AutoSize = true;
            this.TopFolder.Checked = true;
            this.TopFolder.Location = new System.Drawing.Point(158, 52);
            this.TopFolder.Name = "TopFolder";
            this.TopFolder.Size = new System.Drawing.Size(249, 36);
            this.TopFolder.TabIndex = 0;
            this.TopFolder.TabStop = true;
            this.TopFolder.Text = "Top Folder only";
            this.TopFolder.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(213, 365);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 32);
            this.label1.TabIndex = 2;
            this.label1.Text = "Folder:";
            // 
            // Directory
            // 
            this.Directory.AutoSize = true;
            this.Directory.Location = new System.Drawing.Point(532, 365);
            this.Directory.Name = "Directory";
            this.Directory.Size = new System.Drawing.Size(93, 32);
            this.Directory.TabIndex = 3;
            this.Directory.Text = "label2";
            // 
            // ChangeFolderButton
            // 
            this.ChangeFolderButton.Location = new System.Drawing.Point(475, 400);
            this.ChangeFolderButton.Name = "ChangeFolderButton";
            this.ChangeFolderButton.Size = new System.Drawing.Size(202, 49);
            this.ChangeFolderButton.TabIndex = 4;
            this.ChangeFolderButton.Text = "Change Folder";
            this.ChangeFolderButton.UseVisualStyleBackColor = true;
            this.ChangeFolderButton.Click += new System.EventHandler(this.ChangeFolderButton_Click);
            // 
            // FolderBrowser
            // 
            this.FolderBrowser.HelpRequest += new System.EventHandler(this.FolderBrowser_HelpRequest);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(488, 511);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(137, 49);
            this.SaveButton.TabIndex = 5;
            this.SaveButton.Text = "Save Settings";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // labelWarning
            // 
            this.labelWarning.AutoSize = true;
            this.labelWarning.Location = new System.Drawing.Point(681, 511);
            this.labelWarning.Name = "labelWarning";
            this.labelWarning.Size = new System.Drawing.Size(295, 32);
            this.labelWarning.TabIndex = 6;
            this.labelWarning.Text = "Please select a folder!";
            // 
            // Counter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 632);
            this.Controls.Add(this.labelWarning);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.ChangeFolderButton);
            this.Controls.Add(this.Directory);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.EnableCounter);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Counter";
            this.Text = "FileCounter Settings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox EnableCounter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton AllFolders;
        private System.Windows.Forms.RadioButton TopFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Directory;
        private System.Windows.Forms.Button ChangeFolderButton;
        private System.Windows.Forms.FolderBrowserDialog FolderBrowser;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Label labelWarning;
    }
}