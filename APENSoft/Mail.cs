﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using Microsoft.Win32;

namespace APENSoft
{
    public partial class Mail : Form
    {
        //Start Timer

        public Mail()
        {
            //TODO POP3 Client? => Absprache mit David
            InitializeComponent();
            RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\APEN\APENSoft");
            this.txbPassword.Text = Convert.ToString(key.GetValue("SenderPassword"));
            this.txbMachine.Text = Convert.ToString(key.GetValue("ServerName"));
            this.txbRecieverAdress.Text = Convert.ToString(key.GetValue("RecieverMail"));
            this.txbSenderAdress.Text = Convert.ToString(key.GetValue("SenderMail"));
            this.txbServer.Text = Convert.ToString(key.GetValue("SenderServer"));
            this.txbPort.Text = Convert.ToString(key.GetValue("ServerPort"));
            if (Convert.ToString(key.GetValue("MailFeature")) == "1")
            {
                this.MailEnable.Checked = true;
            }
            else
            {
                this.MailEnable.Checked = false;
            }

            Timer clock = new Timer { Interval = 1 };
            clock.Tick += new EventHandler(this.timer_tick);
            clock.Start();
        }

        //Check if all fields are filled in
        private void timer_tick(object sender, EventArgs e)
        {
            bool allfieldsempty = true;

            if (this.MailEnable.Checked)
            {
                bool validData = true;
                foreach (Control control in this.Controls)
                {
                    if (control is TextBox)
                    {
                        TextBox textbox = control as TextBox;
                        validData &= !string.IsNullOrWhiteSpace(textbox.Text);
                    }
                }
                switch (validData)
                {
                    case true:
                        this.WarningFields.Hide();
                        break;

                    case false:
                        this.WarningFields.Show();
                        break;
                }
                this.SaveMail.Enabled = validData;
                this.ConnectionTest.Enabled = validData;
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {
        }

        private void ConnectionChecker(string host, string username, string password, string strPort)
        {
            int port = Convert.ToInt32(strPort);
            var client = new SmtpClient(host, port)
            {
                Credentials = new NetworkCredential(username, password),
                EnableSsl = true
            };
            try
            {
                client.Send(this.txbMachine + "@example.com", "to@example.com", "Mail Connectivity Check", "testbody");
                MessageBox.Show("Mail Connectivity Check Passed!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Mail Connectivity Check Failed!\n {0}", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
        }

        private void ConnectionTest_Click(object sender, EventArgs e)
        {
            this.ConnectionChecker(this.txbServer.Text, this.txbSenderAdress.Text, this.txbPassword.Text, this.txbPort.Text);
        }

        private void SaveMail_Click(object sender, EventArgs e)
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\APEN\APENSoft");
            if (this.MailEnable.Checked == true)
            {
                key.SetValue("MailFeature", "1");
            }
            else
            {
                key.SetValue("MailFeature", "0");
            }
            key.SetValue("RecieverMail", this.txbRecieverAdress.Text);
            key.SetValue("SenderMail", this.txbSenderAdress.Text);
            key.SetValue("SenderPassword", this.txbPassword.Text);
            key.SetValue("SenderServer", this.txbServer.Text);
            key.SetValue("ServerName", this.txbMachine.Text);
            key.SetValue("ServerPort", this.txbPort.Text);
            key.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }
    }
}