﻿namespace APENSoft
{
    partial class Log
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Log));
            this.EnableLog = new System.Windows.Forms.CheckBox();
            this.LogBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.saveButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.Label = new System.Windows.Forms.Label();
            this.PathLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // EnableLog
            // 
            this.EnableLog.AutoSize = true;
            this.EnableLog.Location = new System.Drawing.Point(276, 65);
            this.EnableLog.Name = "EnableLog";
            this.EnableLog.Size = new System.Drawing.Size(198, 36);
            this.EnableLog.TabIndex = 0;
            this.EnableLog.Text = "Enable Log";
            this.EnableLog.UseVisualStyleBackColor = true;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(276, 327);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(234, 81);
            this.saveButton.TabIndex = 1;
            this.saveButton.Text = "Save Settings";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(276, 137);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(234, 81);
            this.button2.TabIndex = 2;
            this.button2.Text = "Select Location";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Label
            // 
            this.Label.AutoSize = true;
            this.Label.Location = new System.Drawing.Point(270, 256);
            this.Label.Name = "Label";
            this.Label.Size = new System.Drawing.Size(201, 32);
            this.Label.TabIndex = 3;
            this.Label.Text = "Selected Path:";
            // 
            // PathLabel
            // 
            this.PathLabel.AutoSize = true;
            this.PathLabel.Location = new System.Drawing.Point(562, 256);
            this.PathLabel.Name = "PathLabel";
            this.PathLabel.Size = new System.Drawing.Size(0, 32);
            this.PathLabel.TabIndex = 4;
            // 
            // Log
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 626);
            this.Controls.Add(this.PathLabel);
            this.Controls.Add(this.Label);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.EnableLog);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Log";
            this.Text = "Log";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox EnableLog;
        private System.Windows.Forms.FolderBrowserDialog LogBrowser;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label Label;
        private System.Windows.Forms.Label PathLabel;
    }
}