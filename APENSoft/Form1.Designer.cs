﻿namespace APENSoft
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileCounterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sizeExplorerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mailSetingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.CounterActive = new System.Windows.Forms.Label();
            this.ExplorerActive = new System.Windows.Forms.Label();
            this.logActive = new System.Windows.Forms.Label();
            this.MailActive = new System.Windows.Forms.Label();
            this.labelCounterDir = new System.Windows.Forms.Label();
            this.labelCounterType = new System.Windows.Forms.Label();
            this.labelLogDir = new System.Windows.Forms.Label();
            this.labelSender = new System.Windows.Forms.Label();
            this.labelReciever = new System.Windows.Forms.Label();
            this.CounterDir = new System.Windows.Forms.Label();
            this.CounterType = new System.Windows.Forms.Label();
            this.LogDir = new System.Windows.Forms.Label();
            this.Sender = new System.Windows.Forms.Label();
            this.Reciever = new System.Windows.Forms.Label();
            this.LaunchButton = new System.Windows.Forms.Button();
            this.liceseInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1255, 49);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileCounterToolStripMenuItem,
            this.sizeExplorerToolStripMenuItem,
            this.mailSetingsToolStripMenuItem,
            this.logSettingsToolStripMenuItem,
            this.liceseInformationToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(137, 45);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // fileCounterToolStripMenuItem
            // 
            this.fileCounterToolStripMenuItem.Name = "fileCounterToolStripMenuItem";
            this.fileCounterToolStripMenuItem.Size = new System.Drawing.Size(375, 46);
            this.fileCounterToolStripMenuItem.Text = "File Counter";
            this.fileCounterToolStripMenuItem.Click += new System.EventHandler(this.fileCounterToolStripMenuItem_Click);
            // 
            // sizeExplorerToolStripMenuItem
            // 
            this.sizeExplorerToolStripMenuItem.Name = "sizeExplorerToolStripMenuItem";
            this.sizeExplorerToolStripMenuItem.Size = new System.Drawing.Size(375, 46);
            this.sizeExplorerToolStripMenuItem.Text = "Size Explorer";
            this.sizeExplorerToolStripMenuItem.Click += new System.EventHandler(this.sizeExplorerToolStripMenuItem_Click);
            // 
            // mailSetingsToolStripMenuItem
            // 
            this.mailSetingsToolStripMenuItem.Name = "mailSetingsToolStripMenuItem";
            this.mailSetingsToolStripMenuItem.Size = new System.Drawing.Size(375, 46);
            this.mailSetingsToolStripMenuItem.Text = "Mail Setings";
            this.mailSetingsToolStripMenuItem.Click += new System.EventHandler(this.mailSetingsToolStripMenuItem_Click);
            // 
            // logSettingsToolStripMenuItem
            // 
            this.logSettingsToolStripMenuItem.Name = "logSettingsToolStripMenuItem";
            this.logSettingsToolStripMenuItem.Size = new System.Drawing.Size(375, 46);
            this.logSettingsToolStripMenuItem.Text = "Log Settings";
            this.logSettingsToolStripMenuItem.Click += new System.EventHandler(this.logSettingsToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(332, 170);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "File Counter";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(332, 302);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(184, 32);
            this.label2.TabIndex = 2;
            this.label2.Text = "Size Explorer";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(332, 366);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 32);
            this.label3.TabIndex = 3;
            this.label3.Text = "Log File";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(332, 480);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(218, 32);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mail Notification";
            // 
            // CounterActive
            // 
            this.CounterActive.AutoSize = true;
            this.CounterActive.Location = new System.Drawing.Point(729, 170);
            this.CounterActive.Name = "CounterActive";
            this.CounterActive.Size = new System.Drawing.Size(140, 32);
            this.CounterActive.TabIndex = 5;
            this.CounterActive.Text = "FileActive";
            // 
            // ExplorerActive
            // 
            this.ExplorerActive.AutoSize = true;
            this.ExplorerActive.Location = new System.Drawing.Point(729, 302);
            this.ExplorerActive.Name = "ExplorerActive";
            this.ExplorerActive.Size = new System.Drawing.Size(156, 32);
            this.ExplorerActive.TabIndex = 6;
            this.ExplorerActive.Text = "Size Active";
            // 
            // logActive
            // 
            this.logActive.AutoSize = true;
            this.logActive.Location = new System.Drawing.Point(729, 366);
            this.logActive.Name = "logActive";
            this.logActive.Size = new System.Drawing.Size(63, 32);
            this.logActive.TabIndex = 7;
            this.logActive.Text = "Log";
            // 
            // MailActive
            // 
            this.MailActive.AutoSize = true;
            this.MailActive.Location = new System.Drawing.Point(729, 480);
            this.MailActive.Name = "MailActive";
            this.MailActive.Size = new System.Drawing.Size(93, 32);
            this.MailActive.TabIndex = 8;
            this.MailActive.Text = "label8";
            // 
            // labelCounterDir
            // 
            this.labelCounterDir.AutoSize = true;
            this.labelCounterDir.Location = new System.Drawing.Point(390, 215);
            this.labelCounterDir.Name = "labelCounterDir";
            this.labelCounterDir.Size = new System.Drawing.Size(128, 32);
            this.labelCounterDir.TabIndex = 9;
            this.labelCounterDir.Text = "Directory";
            // 
            // labelCounterType
            // 
            this.labelCounterType.AutoSize = true;
            this.labelCounterType.Location = new System.Drawing.Point(390, 247);
            this.labelCounterType.Name = "labelCounterType";
            this.labelCounterType.Size = new System.Drawing.Size(186, 32);
            this.labelCounterType.TabIndex = 10;
            this.labelCounterType.Text = "Counter Type";
            // 
            // labelLogDir
            // 
            this.labelLogDir.AutoSize = true;
            this.labelLogDir.Location = new System.Drawing.Point(390, 418);
            this.labelLogDir.Name = "labelLogDir";
            this.labelLogDir.Size = new System.Drawing.Size(124, 32);
            this.labelLogDir.TabIndex = 11;
            this.labelLogDir.Text = "Location";
            // 
            // labelSender
            // 
            this.labelSender.AutoSize = true;
            this.labelSender.Location = new System.Drawing.Point(402, 533);
            this.labelSender.Name = "labelSender";
            this.labelSender.Size = new System.Drawing.Size(107, 32);
            this.labelSender.TabIndex = 12;
            this.labelSender.Text = "Sender";
            // 
            // labelReciever
            // 
            this.labelReciever.AutoSize = true;
            this.labelReciever.Location = new System.Drawing.Point(402, 574);
            this.labelReciever.Name = "labelReciever";
            this.labelReciever.Size = new System.Drawing.Size(127, 32);
            this.labelReciever.TabIndex = 13;
            this.labelReciever.Text = "Reciever";
            // 
            // CounterDir
            // 
            this.CounterDir.AutoSize = true;
            this.CounterDir.Location = new System.Drawing.Point(729, 215);
            this.CounterDir.Name = "CounterDir";
            this.CounterDir.Size = new System.Drawing.Size(71, 32);
            this.CounterDir.TabIndex = 14;
            this.CounterDir.Text = "CDir";
            // 
            // CounterType
            // 
            this.CounterType.AutoSize = true;
            this.CounterType.Location = new System.Drawing.Point(729, 247);
            this.CounterType.Name = "CounterType";
            this.CounterType.Size = new System.Drawing.Size(109, 32);
            this.CounterType.TabIndex = 15;
            this.CounterType.Text = "label15";
            // 
            // LogDir
            // 
            this.LogDir.AutoSize = true;
            this.LogDir.Location = new System.Drawing.Point(729, 418);
            this.LogDir.Name = "LogDir";
            this.LogDir.Size = new System.Drawing.Size(63, 32);
            this.LogDir.TabIndex = 16;
            this.LogDir.Text = "Log";
            // 
            // Sender
            // 
            this.Sender.AutoSize = true;
            this.Sender.Location = new System.Drawing.Point(729, 533);
            this.Sender.Name = "Sender";
            this.Sender.Size = new System.Drawing.Size(109, 32);
            this.Sender.TabIndex = 17;
            this.Sender.Text = "label17";
            // 
            // Reciever
            // 
            this.Reciever.AutoSize = true;
            this.Reciever.Location = new System.Drawing.Point(729, 574);
            this.Reciever.Name = "Reciever";
            this.Reciever.Size = new System.Drawing.Size(109, 32);
            this.Reciever.TabIndex = 18;
            this.Reciever.Text = "label18";
            // 
            // LaunchButton
            // 
            this.LaunchButton.Location = new System.Drawing.Point(499, 676);
            this.LaunchButton.Name = "LaunchButton";
            this.LaunchButton.Size = new System.Drawing.Size(339, 139);
            this.LaunchButton.TabIndex = 19;
            this.LaunchButton.Text = "Start Process now!";
            this.LaunchButton.UseVisualStyleBackColor = true;
            this.LaunchButton.Click += new System.EventHandler(this.LaunchButton_Click);
            // 
            // liceseInformationToolStripMenuItem
            // 
            this.liceseInformationToolStripMenuItem.Name = "liceseInformationToolStripMenuItem";
            this.liceseInformationToolStripMenuItem.Size = new System.Drawing.Size(375, 46);
            this.liceseInformationToolStripMenuItem.Text = "Licese Information";
            this.liceseInformationToolStripMenuItem.Click += new System.EventHandler(this.liceseInformationToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1255, 897);
            this.Controls.Add(this.LaunchButton);
            this.Controls.Add(this.Reciever);
            this.Controls.Add(this.Sender);
            this.Controls.Add(this.LogDir);
            this.Controls.Add(this.CounterType);
            this.Controls.Add(this.CounterDir);
            this.Controls.Add(this.labelReciever);
            this.Controls.Add(this.labelSender);
            this.Controls.Add(this.labelLogDir);
            this.Controls.Add(this.labelCounterType);
            this.Controls.Add(this.labelCounterDir);
            this.Controls.Add(this.MailActive);
            this.Controls.Add(this.logActive);
            this.Controls.Add(this.ExplorerActive);
            this.Controls.Add(this.CounterActive);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "APENSoft";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileCounterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sizeExplorerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mailSetingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logSettingsToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label CounterActive;
        private System.Windows.Forms.Label ExplorerActive;
        private System.Windows.Forms.Label logActive;
        private System.Windows.Forms.Label MailActive;
        private System.Windows.Forms.Label labelCounterDir;
        private System.Windows.Forms.Label labelCounterType;
        private System.Windows.Forms.Label labelLogDir;
        private System.Windows.Forms.Label labelSender;
        private System.Windows.Forms.Label labelReciever;
        private System.Windows.Forms.Label CounterDir;
        private System.Windows.Forms.Label CounterType;
        private System.Windows.Forms.Label LogDir;
        private System.Windows.Forms.Label Sender;
        private System.Windows.Forms.Label Reciever;
        private System.Windows.Forms.Button LaunchButton;
        private System.Windows.Forms.ToolStripMenuItem liceseInformationToolStripMenuItem;
    }
}

