﻿using System;
using System.Windows.Forms;
using Microsoft.Win32;

namespace APENSoft
{
    public partial class Log : Form
    {
        private string _directory;

        public Log()
        {
            this.InitializeComponent();
            RegistryKey reg = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\APEN\APENSoft", true);
            this._directory = Convert.ToString(reg.GetValue("LogDirectory"));
            this.EnableLog.Checked = Convert.ToString(reg.GetValue("LogFeature")) == "1";
            Timer clock = new Timer { Interval = 1 };
            clock.Tick += this.timer_tick;
            clock.Start();
        }

        private void timer_tick(object sender, EventArgs e)
        {
            if (this.EnableLog.Checked && this._directory == "")
            {
                this.saveButton.Enabled = false;
                this.Label.Hide();
                this.PathLabel.Hide();
            }
            else
            {
                this.saveButton.Enabled = true;
                this.Label.Show();
                this.PathLabel.Show();
                this.PathLabel.Text = this._directory;
            }
            if (this.LogBrowser.SelectedPath == null)
            {
            }
            else
            {
                this._directory = this.LogBrowser.SelectedPath;
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            RegistryKey reg = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\APEN\APENSoft", true);
            reg.SetValue("LogFeature", this.EnableLog.Checked ? "1" : "0");
            reg.SetValue("LogDirectory", this._directory);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.LogBrowser.ShowDialog();
        }
    }
}