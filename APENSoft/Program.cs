﻿/*
           _____  ______ _   _  _____        __ _
     /\   |  __ \|  ____| \ | |/ ____|      / _| |
    /  \  | |__) | |__  |  \| | (___   ___ | |_| |_
   / /\ \ |  ___/|  __| | . ` |\___ \ / _ \|  _| __|
  / ____ \| |    | |____| |\  |____) | (_) | | | |_
 /_/    \_\_|    |______|_| \_|_____/ \___/|_|  \__|
 (c) HS|MediaSolutions [2017]
 * This Project is subject to the terms and conditions defined at https://www.binpress.com/license/view/l/5cd047eb990e326cc0682ae694607191
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APENSoft
{
    internal static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            string a = String.Empty;
            if (args.Length > 0)
            {
                a = args[0];
            }
            Application.Run(new Form1(a));
        }
    }
}